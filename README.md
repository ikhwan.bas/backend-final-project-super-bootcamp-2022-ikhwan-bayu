<a href="#"><img width="100%" height="auto" src="https://i.imgur.com/iXuL1HG.png" height="175px"/></a>

<h1 align="center">Hi <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px">, Welcome to KulakPedia E-Commerce Application</h1>

## Backend - Final Project Super Bootcamp - Sanbercode - 2022

## 💫 Description
This is a RESTful API for e-commerce application. This application created in Final Project Super Bootcamp Sanbercode 2022.

## 🌱 Installation 
#### To start this project, run
```
go run main.go
```

Runs the app in the development mode.\
Open [http://localhost:8080](http://localhost:8080) to view it in your browser.

## 📊 Documentation : 

Documentation can be found at /swagger/index.html#/ 

## ER Diagram
<img width="100%" height="auto" src="./ERDiagram.png" height="175px"/>

## 🚀 Languages & Tools:
<p align="left"> 
    <a style="padding-right:8px;" href="https://www.mysql.com/" target="_blank"> <img src="https://img.icons8.com/fluent/50/000000/mysql-logo.png" width="45" height="40"/> </a>
    <a style="padding-right:8px;" href="https://www.postgresql.org/" target="_blank"> <img src="https://www.postgresql.org/media/img/about/press/elephant.png" width="40" height="40"/> </a>
    <a href="https://postman.com" target="_blank"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="45" height="45"/> </a>   
    <a href="https://git-scm.com/" target="_blank"> <img src="https://img.icons8.com/color/48/000000/git.png"/> </a> 
    <a href="https://go.dev/" target="_blank"> <img src="https://img.icons8.com/color/48/000000/golang.png" alt="golang" width="40" height="40"/> </a>
    <a href="https://github.com/gin-gonic/gin" target="_blank"> <img src="https://raw.githubusercontent.com/gin-gonic/logo/master/color.png" alt="gin" width="40" height="40"/> </a>
      <a href="https://jwt.io/" target="_blank"> <img src="https://jwt.io/img/pic_logo.svg" alt="jwt" width="40" height="40"/> </a>
       <a href="https://swagger.io/" target="_blank"> <img src="https://static1.smartbear.co/swagger/media/assets/images/swagger_logo.svg" alt="swagger" width="40" height="40"/> </a>
</p>

## 🚀 Other Dependencies :
- UUID
- Bcrypt
- GOrm
- CORS
- dotenv

## 🚀 Roadmap
Target Development Roadmap : Creating user routes so, people can order a product in our application. 

## ⚡ License
open source projects

## 👨‍ Connect with me:
<p align="left">

<a href = "https://www.linkedin.com/in/ikhwan-bayu-adhi-setiawan/"><img src="https://img.icons8.com/fluent/48/000000/linkedin.png"/></a>
<a href = "https://twitter.com/Ikhwan_IBAS"><img src="https://img.icons8.com/fluent/48/000000/twitter.png"/></a>
<a href = "https://www.instagram.com/ikhwanbas/"><img src="https://img.icons8.com/fluent/48/000000/instagram-new.png"/></a>
<a href = "https://www.youtube.com/channel/UCpwIHV2ECKm7dmNu83muqWQ"><img src="https://img.icons8.com/color/48/000000/youtube-play.png"/></a>

</p>


