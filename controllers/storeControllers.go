package controllers

import (
	"ecommerce/models"
	"ecommerce/utils/token"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// UpdateStoreProfile godoc
// @Summary Update Store Profile By Role Store.
// @Description Store can update their store profile like address, city, phone number, and postal code.
// @Tags Store Routes
// @Param Authorization header string true "Store Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Accept json
// @Produce json
// @Success 200 {object} models.StoresProfile
// @Failure 400 {object} models.Errorhandling
// @Router /store/profile [put]
func UpdateStoreProfile(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	id, _ := token.ExtractTokenID(c)

	// Get model if exist
	var storeProfile models.StoresProfile
	if err := db.Where("id = ?", id).First(&storeProfile).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	// Validate input
	var input models.StoreProfileInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// Update Store Profile
	var newProfile models.StoresProfile
	newProfile.Address = input.Address
	newProfile.PhoneNumber = input.PhoneNumber
	newProfile.City = input.City
	newProfile.PostalCode = input.PostalCode

	// input to database
	var err error = db.Model(&storeProfile).Updates(newProfile).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": storeProfile})
}

// GetStoreProfile godoc
// @Summary Get Store Profile By Role Store.
// @Description Store can get their store profile like address, city, phone number, and postal code.
// @Tags Store Routes
// @Param Authorization header string true "Store Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.StoresProfile
// @Failure 400 {object} models.Errorhandling
// @Router /store/profile [get]
func GetStoreProfile(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	id, _ := token.ExtractTokenID(c)

	var storeProfile []models.StoresProfile
	db.First(&storeProfile, id)

	c.JSON(http.StatusOK, gin.H{"data": storeProfile})
}

// GetStoreById godoc
// @Summary Get Store Data By Role Store.
// @Description Store can get their store data like storename, email, hashed password.
// @Tags Store Routes
// @Param Authorization header string true "Store Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Stores
// @Failure 400 {object} models.Errorhandling
// @Router /store [get]
func GetStoreById(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	id, _ := token.ExtractTokenID(c)

	var store []models.Stores
	db.First(&store, id)

	c.JSON(http.StatusOK, gin.H{"data": store})
}

// GetStoreByParamId godoc
// @Summary Get Store Data By Store ID.
// @Description Public can get store information data like store name and email.
// @Tags Public Routes
// @Produce json
// @Param id path string true "Store ID (UUID)"
// @Success 200 {object} models.Stores
// @Failure 400 {object} models.Errorhandling
// @Router /store/{id} [get]
func GetStoreByParamId(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var store []models.Stores
	db.First(&store, "id = ?", c.Param("id"))

	c.JSON(http.StatusOK, gin.H{"data": store})
}

// GetStoreProfileByParamId godoc
// @Summary Get Store Profile Data By Store ID.
// @Description Public can get store information data like address, city, phone number, and postal code..
// @Tags Public Routes
// @Produce json
// @Param id path string true "Store ID (UUID)"
// @Success 200 {object} models.StoresProfile
// @Failure 400 {object} models.Errorhandling
// @Router /store/profile/{id} [get]
func GetStoreProfileByParamId(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var storeProfile []models.StoresProfile
	db.First(&storeProfile, "id = ?", c.Param("id"))

	c.JSON(http.StatusOK, gin.H{"data": storeProfile})
}
