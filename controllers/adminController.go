package controllers

import (
	"ecommerce/models"
	"ecommerce/utils/token"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// RegisterAdmin godoc
// @Summary Register an account as admin.
// @Description Registering by fullname, email, and password.
// @Tags Admin Routes
// @Param Body body models.AdminRegisterInput true "the body to register an admin"
// @Accept json
// @Produce json
// @Success 200 {object} models.RegisterAdmin200
// @Failure 400 {object} models.Errorhandling
// @Router /admin/register [post]
func RegisterAdmin(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Validate input
	var input models.AdminRegisterInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create User
	admin := models.Admin{
		Fullname: input.Fullname,
		Email:    input.Email,
		Password: input.Password,
	}

	_, err := admin.SaveAdmin(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"fullname": admin.Fullname, "email": admin.Email})
}

// LoginAdmin godoc
// @Summary Login Admin Account.
// @Description Logging in to get jwt token to access admin or user api by roles admin.
// @Tags Admin Routes
// @Param Body body models.AdminLoginInput true "the body to login as admin"
// @Accept json
// @Produce json
// @Success 200 {object} models.LoginAdmin200
// @Failure 400 {object} models.Errorhandling
// @Router /admin/login [post]
func LoginAdmin(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.AdminLoginInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	a := models.Admin{}

	a.Email = input.Email
	a.Password = input.Password

	token, err := models.AdminLoginCheck(a.Email, a.Password, db)

	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "username or password is incorrect."})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "login success", "token": token})

}

// GetAdminData godoc
// @Summary Get Admin Data like fullname and email using bearer token.
// @Description Get Admin Data like fullname and email.
// @Tags Admin Routes
// @Param Authorization header string true "Admin Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.RegisterAdmin200
// @Failure 400 {object} models.Errorhandling
// @Router /admin [get]
func GetAdminData(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var admin []models.Admin

	id, _ := token.ExtractTokenID(c)

	db.First(&admin, "id = ?", id)

	c.JSON(http.StatusOK, gin.H{"data": admin})
}

// PostProduct godoc
// @Summary Post Product by Roles Admin.
// @Description Admin can post product that was submitted by store.
// @Tags Admin Routes
// @Param Authorization header string true "Admin Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param name path string true "Product Status: post / unpost"
// @Param id path string true "Product ID (UUID)"
// @Produce json
// @Success 200 {string} success
// @Failure 400 {object} models.Errorhandling
// @Router /admin/product/{name}/{id} [put]
func PostProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Get model if exist
	var product models.Products
	if err := db.Where("id = ?", c.Param("id")).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	if c.Param("name") == "post" {
		// input to database
		var err error = db.Model(&product).Update("post_status", "posted").Error
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
	} else if c.Param("name") == "unpost" {
		// input to database
		var err error = db.Model(&product).Update("post_status", "deleted").Error
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
	}
	c.JSON(http.StatusOK, gin.H{"data": product})
}

// GetAllStore godoc
// @Summary Get All Stores Data by Roles Admin.
// @Description Admin can get all store data.
// @Tags Admin Routes
// @Param Authorization header string true "Admin Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {string} models.Store
// @Failure 400 {object} models.Errorhandling
// @Router /admin/store [get]
func GetAllStore(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var stores []models.Stores
	db.Find(&stores)

	c.JSON(http.StatusOK, gin.H{"data": stores})
}
