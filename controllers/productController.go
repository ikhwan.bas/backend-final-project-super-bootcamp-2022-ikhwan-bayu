package controllers

import (
	"ecommerce/models"
	"ecommerce/utils/token"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

// CreateNewProduct godoc
// @Summary Create New Product Data By Role Store.
// @Description Store can create a new product data.
// @Tags Product Routes
// @Param Authorization header string true "Store Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Accept json
// @Produce json
// @Success 200 {object} models.Products
// @Failure 400 {object} models.Errorhandling
// @Router /store/products [post]
func CreateNewProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// validate token as store id
	id, _ := token.ExtractTokenID(c)
	s := models.Stores{}
	err := db.Model(s).Where("ID = ?", id).Take(&s).Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// get first admin id
	var a models.Admin
	err = db.Model(a).First(&a).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Validate Product Input
	var inputProduct models.ProductInput
	if err := c.ShouldBindJSON(&inputProduct); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Product
	product := models.Products{
		ID:                uuid.New(),
		ProductName:       inputProduct.ProductName,
		Stock:             inputProduct.Stock,
		Price:             inputProduct.Price,
		Weight:            inputProduct.Weight,
		Description:       inputProduct.Description,
		ImageURL:          inputProduct.ImageURL,
		ProductCategoryID: inputProduct.ProductCategoryID,
		StoresID:          id,
		PostStatus:        "pending",
		AdminID:           a.ID,
	}

	err = db.Create(&product).Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": "success"})
}

// UpdateProduct godoc
// @Summary Update Product Data By Role Store.
// @Description Store can update product data.
// @Tags Product Routes
// @Param Authorization header string true "Store Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param id path string true "Product ID (UUID)"
// @Accept json
// @Produce json
// @Success 200 {object} models.Products
// @Failure 400 {object} models.Errorhandling
// @Router /store/products/{id} [put]
func UpdateProduct(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Get model if exist
	var product models.Products
	if err := db.Where("id = ?", c.Param("id")).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	// Validate input
	var update models.ProductInput
	if err := c.ShouldBindJSON(&update); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Update Product
	var newProduct models.Products
	newProduct.ProductName = update.ProductName
	newProduct.Stock = update.Stock
	newProduct.Price = update.Price
	newProduct.Weight = update.Weight
	newProduct.Description = update.Description
	newProduct.ImageURL = update.ImageURL
	newProduct.ProductCategoryID = update.ProductCategoryID

	// input to database
	var err error = db.Model(&product).Updates(newProduct).Error
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": product})
}

// DeleteProduct godoc
// @Summary Delete Product Data By Role Store.
// @Description Store can delete product data.
// @Tags Product Routes
// @Param Authorization header string true "Store Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param id path string true "Product ID (UUID)"
// @Produce json
// @Success 200 {string} delete success
// @Failure 400 {object} models.Errorhandling
// @Router /store/products/{id} [delete]
func DeleteProduct(c *gin.Context) {
	// Get model if exist
	db := c.MustGet("db").(*gorm.DB)
	var product models.Products
	if err := db.Where("id = ?", c.Param("id")).First(&product).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&product)

	c.JSON(http.StatusOK, gin.H{"message": "delete success"})
}

// GetAllProduct godoc
// @Summary Public can get all product data.
// @Description Public can get all product data
// @Tags Public Routes
// @Produce json
// @Success 200 {object} models.Products
// @Failure 400 {object} models.Errorhandling
// @Router /products [get]
func GetAllProduct(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	var product []models.Products
	err := db.Find(&product).Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": product})
}

// GetAllProductByCategoryID godoc
// @Summary Public can get all product data by category ID.
// @Description Public can get all product data by category ID
// @Tags Public Routes
// @Produce json
// @Param id path string true "Category ID (UUID)"
// @Success 200 {object} models.Products
// @Failure 400 {object} models.Errorhandling
// @Router /products/category/{id} [get]
func GetProductByCategoryID(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	var product []models.Products
	err := db.Where("product_category_id = ?", c.Param("id")).Find(&product).Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": product})
}

// GetAllProductByStoreID godoc
// @Summary Store can get all of their product data .
// @Description Store can get all product data by Store ID
// @Tags Product Routes
// @Produce json
// @Param Authorization header string true "Store Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Products
// @Failure 400 {object} models.Errorhandling
// @Router /store/products [get]
func GetAllProductByStoreID(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	// validate token as store id
	id, _ := token.ExtractTokenID(c)

	var product []models.Products
	err := db.Where("stores_id = ?", id).Find(&product).Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": product})
}

// GetProductById godoc
// @Summary Public can get all product data by product ID.
// @Description Public can get all product data by product ID
// @Tags Public Routes
// @Produce json
// @Param id path string true "Product ID (UUID)"
// @Success 200 {object} models.Products
// @Failure 400 {object} models.Errorhandling
// @Router /products/{id} [get]
func GetProductById(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	var product []models.Products
	err := db.Set("gorm:auto_preload", true).Preload("Stores.StoresProfile").Preload("ProductCategory").First(&product, "id = ?", c.Param("id")).Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": product})
}

// GetAllProductByCategoryID godoc
// @Summary Public can get all product data by category ID.
// @Description Public can get all product data by category ID
// @Tags Public Routes
// @Produce json
// @Param name path string true "search product name"
// @Success 200 {object} models.Products
// @Failure 400 {object} models.Errorhandling
// @Router /products/search/{name} [get]
func SearchProduct(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)

	var product []models.Products

	err := db.Set("gorm:auto_preload", true).Preload("Stores.StoresProfile").Preload("ProductCategory").Find(&product, "product_name LIKE ?", "%"+c.Param("name")+"%").Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": product})
}
