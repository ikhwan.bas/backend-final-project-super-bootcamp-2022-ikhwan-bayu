package controllers

import (
	"ecommerce/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

// InputProductCategory godoc
// @Summary Create New Product Category by Roles Admin.
// @Description Admin can create new product category.
// @Tags Admin Routes
// @Param Admin Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {string} models.ProductCategory
// @Failure 400 {object} models.Errorhandling
// @Router /admin/products-category [post]
func InputProductCategory(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Validate input
	var inputCategory models.ProductCategoryInput
	if err := c.ShouldBindJSON(&inputCategory); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Product Category
	productCategory := models.ProductCategory{
		ID:           uuid.New(),
		CategoryName: inputCategory.CategoryName,
	}

	var err error = db.Create(&productCategory).Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"category_name": productCategory.CategoryName})
}

// GetProductCategory godoc
// @Summary Get All Product Category Data by Public.
// @Description Public can get all category data.
// @Tags Public Routes
// @Produce json
// @Success 200 {string} models.ProductCategory
// @Failure 400 {object} models.Errorhandling
// @Router /product-category [get]
func GetProductCategory(c *gin.Context) {
	// get db from gin context
	db := c.MustGet("db").(*gorm.DB)
	var productCategory []models.ProductCategory
	db.Find(&productCategory)

	c.JSON(http.StatusOK, gin.H{"data": productCategory})
}
