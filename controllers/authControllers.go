package controllers

import (
	"ecommerce/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// !IMPORTANT :This function is not used yet
// ###### THIS FILE IS CREATED FOR THE NEXT DEVELOPMENT VERSION ######

func RegisterUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Validate input
	var input models.RegisterInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create User
	user := models.Users{
		Username: input.Username,
		Email:    input.Email,
		Password: input.Password,
	}

	_, err := user.SaveUser(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"username": user.Username, "email": user.Email})
}

func LoginUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.LoginInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.Users{}

	u.Username = input.Username
	u.Password = input.Password
	u.Email = input.Email

	token, err := models.LoginCheck(u.Email, u.Username, u.Password, db)

	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "username or password is incorrect."})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "login success", "token": token})

}
