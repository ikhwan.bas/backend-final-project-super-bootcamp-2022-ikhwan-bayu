package controllers

import (
	"ecommerce/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// RegisterStore godoc
// @Summary Register a store account.
// @Description Registering by store name, email, and password.
// @Tags Store Routes
// @Param Body body models.StoreRegisterInput true "the body to register a store account"
// @Accept json
// @Produce json
// @Success 200 {object} models.RegisterStore200
// @Failure 400 {object} models.Errorhandling
// @Router /store/register [post]
func RegisterStore(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Validate input
	var inputStore models.StoreRegisterInput
	if err := c.ShouldBindJSON(&inputStore); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create User
	store := models.Stores{
		StoreName: inputStore.StoreName,
		Email:     inputStore.Email,
		Password:  inputStore.Password,
	}

	_, err := store.SaveStore(db)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"store_name": store.StoreName, "email": store.Email})
}

// LoginStore godoc
// @Summary Login Store Account.
// @Description Logging in to get jwt token to access store api by roles store.
// @Tags Store Routes
// @Param Body body models.StoreLoginInput true "the body to login a store account"
// @Accept json
// @Produce json
// @Success 200 {object} models.LoginAdmin200
// @Failure 400 {object} models.Errorhandling
// @Router /store/login [post]
func LoginStore(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var input models.StoreLoginInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	s := models.Stores{}

	s.Password = input.Password
	s.Email = input.Email

	token, err := models.StoreLoginCheck(s.Email, s.Password, db)

	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "email or password is incorrect."})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "login success", "token": token})

}
