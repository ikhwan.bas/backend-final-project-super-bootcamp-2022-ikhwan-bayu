package models

// Response TO Swagger documentation
type (
	RegisterAdmin200 struct {
		Fullname string `json:"fullname"`
		Email    string `json:"email"`
	}

	LoginAdmin200 struct {
		Message string `default:"login success" json:"message"`
		Token   string `default:"token value" json:"token"`
	}

	Errorhandling struct {
		Error string `default:"error-message" json:"error"`
	}

	RegisterStore200 struct {
		StoreName string `json:"store_name"`
		Email     string `json:"email"`
	}
)
