package models

import (
	"ecommerce/utils/token"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type (
	Admin struct {
		ID       uuid.UUID `gorm:"primary_key" json:"id"`
		Fullname string    `json:"fullname"`
		Email    string    `json:"email"`
		Password string    `json:"password"`

		ProductsID []Products
	}

	AdminRegisterInput struct {
		Fullname string `json:"fullname"`
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	AdminLoginInput struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}
)

// function for register and login
func AdminVerifyPassword(password, hashedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func AdminLoginCheck(email string, password string, db *gorm.DB) (string, error) {

	var err error

	a := Admin{}

	err = db.Model(Admin{}).Where("email = ?", email).Take(&a).Error

	if err != nil {
		return "", err
	}

	err = AdminVerifyPassword(password, a.Password)

	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", err
	}

	token, err := token.GenerateToken(a.ID)

	if err != nil {
		return "", err
	}

	return token, nil

}

func (a *Admin) SaveAdmin(db *gorm.DB) (*Admin, error) {
	//turn password into hash
	hashedPassword, errPassword := bcrypt.GenerateFromPassword([]byte(a.Password), bcrypt.DefaultCost)
	if errPassword != nil {
		return &Admin{}, errPassword
	}
	// generate uuid
	a.ID = uuid.New()
	// hashed password
	a.Password = string(hashedPassword)

	// create user data
	var err error = db.Create(&a).Error
	if err != nil {
		return &Admin{}, err
	}
	return a, nil
}
