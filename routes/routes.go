package routes

import (
	"ecommerce/controllers"
	"ecommerce/middlewares"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()
	r.SetTrustedProxies([]string{"localhost"})

	// CORS Setting
	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowHeaders = []string{"Content-Type", "X-XSRF-TOKEN", "Accept", "Origin", "X-Requested-With", "Authorization"}
	// To be able to send tokens to the server.
	corsConfig.AllowCredentials = true
	// OPTIONS method for ReactJS
	corsConfig.AddAllowMethods("OPTIONS")
	r.Use(cors.New(corsConfig))

	// set db to gin context
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	// Routes with JWT
	jwt := r.Group("/")
	jwt.Use(middlewares.JwtAuthMiddleware())

	// ROUTES
	// ADMIN routes
	r.POST("/admin/register", controllers.RegisterAdmin)
	r.POST("/admin/login", controllers.LoginAdmin)

	jwt.GET("/admin", controllers.GetAdminData)
	jwt.PUT("/admin/product/:name/:id", controllers.PostProduct)

	jwt.GET("/admin/store", controllers.GetAllStore)

	// Store Routes
	r.POST("/store/register", controllers.RegisterStore)
	r.POST("/store/login", controllers.LoginStore)

	jwt.PUT("/store/profile", controllers.UpdateStoreProfile)
	jwt.GET("/store/profile", controllers.GetStoreProfile)
	jwt.GET("/store", controllers.GetStoreById)

	r.GET("/store/:id", controllers.GetStoreByParamId)
	r.GET("/store/profile/:id", controllers.GetStoreProfileByParamId)

	// PRODUCT Routes
	jwt.POST("/admin/products-category", controllers.InputProductCategory)

	r.GET("/product-category", controllers.GetProductCategory)

	jwt.POST("/store/products", controllers.CreateNewProduct)
	jwt.PUT("/store/products/:id", controllers.UpdateProduct)
	jwt.DELETE("/store/products/:id", controllers.DeleteProduct)

	r.GET("/products", controllers.GetAllProduct)
	r.GET("/products/category/:id", controllers.GetProductByCategoryID)

	jwt.GET("/store/products", controllers.GetAllProductByStoreID)

	r.GET("/product/:id", controllers.GetProductById)

	r.GET("/product/search/:name", controllers.SearchProduct)

	// swagger routes
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// !IMPORTANT : THIS ROUTE IS NOT USED YET
	// *** { Progress for the next version }

	// r.GET("/admin/user", controllers.GetAllUser)
	// r.POST("/register", controllers.RegisterUser)
	// r.POST("/login", controllers.LoginUser)

	// jwt.PUT("/profile", controllers.UpdateUserProfile)

	// { Progress for the next version }
	// CART Routes
	// jwt.GET("/cart", controllers.GetAllCart)
	// jwt.GET("/cart/:ids", controllers.GetCartByUserIdAndStoreId)
	// jwt.POST("/cart", controllers.CreateNewCart)
	// jwt.PUT("/cart/:id", controllers.UpdateCart)
	// jwt.DELETE("/cart/:id", controllers.DeleteCart)

	return r
}
